pipeline {
    agent any
    parameters {
        string(name: 'PATH', defaultValue: 'portal/admin/apps/app1', description: 'Repository path to deploy')
        string(name: 'HOST', defaultValue: 'ec2-54-78-189-82.eu-west-1.compute.amazonaws.com', description: 'Remote host to deploy code')
        string(name: 'CREDENTIALS_ID', defaultValue: 'remote-server', description: 'Credentials to be used.')
    }
    environment { 
        remoteHost = "${params.HOST}"
        targetPath = "${params.PATH}"
        remoteCredId = "${params.CREDENTIALS_ID}"
    }
    stages {       
        stage('Release') {
            steps {
                zip zipFile: 'build.zip', archive: true, dir: "${targetPath}"
            }
        }
        stage('Deploy') {
            steps {
                withCredentials([sshUserPrivateKey(credentialsId: "${remoteCredId}", usernameVariable: 'userName', keyFileVariable: 'keyFile')]) {
                    script {
                        def remote = [:]
                        remote.name = remoteHost
                        remote.host = remoteHost
                        remote.user = userName
                        remote.identityFile = keyFile
                        remote.allowAnyHosts = true
                        def remotePath = "/var/www/html/" + targetPath

                        echo "Deploying on server ${remote.host} to user ${remote.user} at path ${remotePath}"
                        sshCommand remote: remote, sudo: true, command: "rm -Rf ${remotePath}/controladores"
                        sshPut remote: remote, sudo: true, from: "./${targetPath}/controladores", into: "${remotePath}"
                        sshCommand remote: remote, sudo: true, command: "rm -Rf ${remotePath}/vistas"
                        sshPut remote: remote, sudo:true, from: "./${targetPath}/vistas", into: "${remotePath}"
                        sshPut remote: remote, override:true, sudo: true, from: "./${targetPath}/ficheros", into: "${remotePath}"
                    }
                }
            }
        }
    }
    post {
        always {
            cleanWs()
        }
    }
}
